-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2016 at 05:29 PM
-- Server version: 5.7.12-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mvcdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `iditem` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `img` varchar(256) DEFAULT NULL,
  `price` float DEFAULT NULL,
  PRIMARY KEY (`iditem`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`iditem`, `name`, `description`, `category`, `img`, `price`) VALUES
(1, 'Чашка', 'Чашка, че', 'Посуда', 'files/1.jpg', 35),
(2, 'Бачок', 'мусорное ведро', 'Бытовая техника', 'files/2.jpg', 28);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  `pass` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `pass`) VALUES
(10, 'user', '$2y$10$ds8HOxaCSgMIXxQEAPFeg.8UhR/qImwAu1gHj3Q.naBzxqGbUzgL2'),
(12, '123@123.com', '$2y$10$r97tNmV1/VH4iNBW5vYpzuVCoZOXDP/4AUlKxWUE3I/5Fxnnu1tA.'),
(13, '123@123.compass=jhjh', '$2y$10$1FyOIDM2OeFMjEFp0UKF7.UFZET67sWO45bPLwTd9Bh505hme6J1S'),
(14, '1234@123.compass=', '$2y$10$lEFG1qRyym5.R6M.4ugfiuwJg87e4CQH7f.yDSnIk5/OSar3wA7sm'),
(15, '12345@123.com', '$2y$10$kYI6gazyVOn6MW8awleaQ.F44h2aWJAZ9cTa0Xll7IjgIW5mNoxM6'),
(16, '123456@123.com', '$2y$10$pVQNbW0/E26P7KOCzA6ZeuK50YvvM1WGYCHUhA1wD4QxKV3y6upja'),
(17, '1234567@123.com', '$2y$10$dhv39zvXkEPoTLp7j1Fco.4dPgZUUhTe0Zbfir.XNGAPrcioin/em'),
(18, '12345678@123.com', '$2y$10$t4wYZ293BeAbHdDDC909h.s7/VnGAdzxwuosKMIh23bik9jegObLS'),
(19, '123456789@123.com', '$2y$10$fkiwqi3YSj0.snpTARera.i5Z7AoGfkAFqI.d6JF0MzdOMj2xHG4S'),
(20, '1@1.com', '$2y$10$Jg9u.lajhHUL0nbiYW7h1ekvH9me2KrdpmX5/HkUuNEqaFDbagy0S'),
(21, '2@2.com', '$2y$10$kNXHwbQrYFDkjuMG108vg..lvrIrZHjl1Kf/vWoosp56hm8X6TPeS'),
(22, 'test', '$2y$10$QXJix8jSspCkd6wri1LXi.OlQR6SRx.mKCjAluI2b0KpMjrslYapu'),
(23, 'test3@gmail.com', '$2y$10$Ds8eVMc7Gz..UdG9UufE.elDacMiqkFZI3VPWvsGK/xKnzvmWonUm'),
(24, 'test@gmail.com', '$2y$10$7kRZGD8aPPt34SyZyzXuJucF2k3nBJgQxuV.hrY8oxQLnLYAfUoem');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
