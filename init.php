<?php
require_once 'app/Log.php';
require_once 'app/services/security/security.php';
require_once 'app/services/security/account.php';
class Server
{
    private static $isInitialized = false;
    public static function init()
    {
        if (self::$isInitialized) return;

        Logger::$enabled = true;
        Security::init(new AccountService());
        
        self::$isInitialized = true;
    }
}