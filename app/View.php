<?php
class View
{
    public static function layout($title, $content = null, $template = 'Template', $model = null)
    {
        include 'app/views/' . $template . '.php';
    }
}