<?php
interface Router
{
    public function route($url);
}
