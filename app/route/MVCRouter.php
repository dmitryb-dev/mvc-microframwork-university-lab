<?php
require_once 'RouterInterface.php';
require_once 'MVCParams.php';

class MVCRouter implements Router
{

    public function getInstance() {
        if (self::$instance === null)
            self::$instance = new MVCRouter();
        return self::$instance;
    }

    public function route($url)
    {
        $params = MVCParams::from_url($url);

        $controller_file = self::$controllers_directory . $params->getControllerName() . '.php';
        if (file_exists($controller_file))
        {
            include $controller_file;
            $controller_name = $params->getControllerName();
            $methodName = $params->getMethodName();

            $controller = new $controller_name();
            if (method_exists($controller, $methodName))
            {
                $controller->$methodName();
                return;
            }
        }
        $this->errorPage404();
    }

    private static
        $controllers_directory = './app/controllers/';

    private static $instance;
    private function __construct() {}

    private function errorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }
}


