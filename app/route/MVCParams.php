<?php
class MVCParams
{
    public static function from_url($url)
    {
        $url_array = explode('/', $url);
        $url_array_length = count($url_array);

        if (trim($url_array[$url_array_length - 1]) == "") $url_array_length--;
        if ($url_array_length < 2) return null;

        $mvcParams = new MVCParams();
        $mvcParams->controllerName = $url_array[$url_array_length - 2];

        $actionArray = explode('?', $url_array[$url_array_length - 1]);
        $mvcParams->methodName = $actionArray[0];
        $mvcParams->params = count($actionArray) > 1? self::parseParams($actionArray[1]) : null;
        return $mvcParams;
    }

    private static function parseParams($actionString) {
        $paramsArray = explode('&', $actionString);
        $paramsParsed = array();
        foreach ($paramsArray as $paramString) {
            $name_value_pair = explode('=', $paramString);
            $paramsParsed[$name_value_pair[0]] = trim($name_value_pair[1]);
        }
        return $paramsParsed;
    }

    private $controllerName, $methodName, $params;

    public function getControllerName()
    {
        return $this->controllerName;
    }

    public function getMethodName()
    {
        return $this->methodName;
    }

    public function getParams()
    {
        return $this->params;
    }

    private function __construct() {}
}