<?php
class Logger
{
    private static $string;

    public static function log($info)
    {
        if (self::$enabled) self::$string = self::$string . "<br>" . $info;
    }

    public static function clear()
    {
        self::$string = self::$HEADER;
    }

    public static function getLogText()
    {
        return self::$string;
    }

    public static $enabled = true;

    private static $HEADER = '<p>LOGS:';
}