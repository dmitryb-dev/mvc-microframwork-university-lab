<?php
require_once 'app/dao/CartDAOCookie.php';
class CartService
{
    public function __construct()
    {
        $this->cartDAO = new CartDAOCookie();
        $this->itemsDAO = new ItemDAOImpl();
    }

    public function getItems()
    {
        $itemIDs = $this->cartDAO->forUser()->getItems();
        $items = array();
        foreach ($itemIDs as $id)
        {
            $item = $this->itemsDAO->load($id);
            if ($item != null)
                $items[] = $item;
        }
        return $items;
    }

    public function addItem($id)
    {
        $cart = $this->cartDAO->forUser();
        $cart->addItem($id);
        $this->cartDAO->save($cart);
    }

    public function getContactsForUser($idUser = null)
    {
        $cart = $this->cartDAO->forUser();
        return $cart->getContact();
    }
    public function setContactsForUser($contact, $idUser = null)
    {
        $cart = $this->cartDAO->forUser();
        $cart->setContact($contact);
        $this->cartDAO->save($cart);
    }

    public function removeItem($id)
    {
        $cart = $this->cartDAO->forUser();
        $cart->removeItem($id);
        $this->cartDAO->save($cart);
    }

    public function clear($id)
    {
        $cart = $this->cartDAO->forUser();
        $cart->clear();
        $this->cartDAO->save($cart);
    }

    private $cartDAO, $itemsDAO;
}