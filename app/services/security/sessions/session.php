<?php
interface SessionService
{
    /*
     * $sessionObject - can contain session data like user permissions for example.
     * returns string - token of session that used for authorization.
     */
    public function createSession($sessionObject);
    public function getSession($token);
}

interface Session
{
    public function getPermissions();
}

class Token
{
    public $id, $value;

    public function __construct($id, $value)
    {
        $this->id = $id;
        $this->value = $value;
    }

    private static
        $COOKIE_TOKEN_ID = 'token_id',
        $COOKIE_TOKEN = 'token';

    public function saveToCookie()
    {
        setcookie(self::$COOKIE_TOKEN_ID, $this->id, time() + (60 * 60 * 24 * 200), '/');
        setcookie(self::$COOKIE_TOKEN, $this->value, time() + (60 * 60 * 24 * 200), '/');
    }

    public static function removeFromCookie()
    {
        setcookie(self::$COOKIE_TOKEN_ID, '', time() - 3600, '/');
        setcookie(self::$COOKIE_TOKEN, '', time() - 3600, '/');
    }

    public static function fromCookie()
    {
        if (isset($_COOKIE[self::$COOKIE_TOKEN_ID]))
        {
            return new Token($_COOKIE[self::$COOKIE_TOKEN_ID], $_COOKIE[self::$COOKIE_TOKEN]);
        }
        return null;
    }
}