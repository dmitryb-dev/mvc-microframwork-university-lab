<?php
require_once 'session.php';
require_once 'session_permissions.php';
require_once 'app/dao/userDAOImpl.php';
/*
 * This class implements sessions... without sessions. Each users has only one session.
 * Token is given by user's pass.
 * This token is reusable and not recommend for system that require high security level.
 */
class PermanentSessionService implements SessionService
{
    public function __construct()
    {
        $this->userDAO = new UserDAOImpl();
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function createSession($sessionObject = null)
    {
        return new Token($this->user->id, $this->createTokenPass($this->user->pass));
    }

    public function getSession($token)
    {
        $user = $this->userDAO->loadById($token->id);
        if ($user != null && $this->createTokenPass($user->pass) === $token->value) return new SessionPermissions($user->role);
        return null;
    }

    private static $salt = 'f43f34f3908g4yw3gf';
    private function createTokenPass($pass)
    {
        return md5($pass . $this->salt);
    }

    private $userDAO;
    private $user;
}