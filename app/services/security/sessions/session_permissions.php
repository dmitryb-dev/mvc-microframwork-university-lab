<?php
require_once 'session.php';
class SessionPermissions implements Session
{
    public function __construct($role)
    {
        $this->role = $role;
    }

    public function getPermissions()
    {
        return $this->role;
    }

    private $role;
}