<?php
require_once 'user_permission.php';
class UserBase
{
    public $id, $email, $pass, $role;

    public function __construct($id = 0, $email, $pass)
    {
        $this->id = $id;
        $this->email = $email;
        $this->pass = $pass;
        $this->role = Permission::$USER;
    }
}