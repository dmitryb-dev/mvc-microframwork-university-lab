<?php
class Permission
{
    public static
        $BASE = 0x1,
        $USER = 0x3,
        $ADMIN = 0x7;

    public static function hasAccess($permission, $required_permissions)
    {
        return $permission & $required_permissions === $required_permissions;
    }

    public static function toString($permission)
    {
        switch ($permission)
        {
            case self::$BASE: return 'BASE';
            case self::$USER: return 'USER';
            case self::$ADMIN: return 'ADMIN';
        }
        return 'Unknown permission: ' . $permission;
    }

}