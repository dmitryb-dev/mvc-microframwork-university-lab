<?php
require_once 'sessions/session.php';
require_once 'app/Log.php';
class Security
{
    public static function init($accountService)
    {
        self::$accountService = $accountService;
    }

    public static function permit($permissions)
    {
        Logger::log('SECURITY: this action require permissions: ' . Permission::toString($permissions));

        $token = Token::fromCookie();
        if ($token === null) return false;
        Logger::log('SECURITY: token: ' . $token->id . ', ' . $token->value);

        $session = self::$accountService->authenticate($token);
        if ($session === null) return false;
        Logger::log('SECURITY: current user permissions: ' . Permission::toString($session->getPermissions()));

        return Permission::hasAccess($session->getPermissions(), $permissions);
    }

    private static $accountService;
}