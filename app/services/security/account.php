<?php
require_once 'app/dao/userDAOImpl.php';
require_once 'passwords.php';
require_once 'app/services/security/sessions/permanent_session.php';
require_once 'app/Log.php';
class AccountService
{
    public function __construct()
    {
        $this->userDAO = new UserDAOImpl();
        $this->sessionService = new PermanentSessionService();
    }

    public function authenticate($token)
    {
        return $this->sessionService->getSession($token);
    }

    public function signIn($email, $pass)
    {
        $user = $this->userDAO->load($email);
        if ($user === null || !password_verify($pass, $user->pass)) return null;
        $this->sessionService->setUser($user);
        return $this->sessionService->createSession();
    }

    public function register($user)
    {
        $existing_user = $this->userDAO->load($user->email);
        if ($existing_user != null) return false;

        $user->pass = password_hash($user->pass, PASSWORD_BCRYPT);
        $this->userDAO->add($user);
        return true;
    }

    private $userDAO;
    private $sessionService;
}