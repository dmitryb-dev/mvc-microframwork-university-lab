<?php
require_once 'app/dao/itemDAOImpl.php';
class ItemsService
{
    public function __construct()
    {
        $this->itemsDAO = new ItemDAOImpl();
    }

    public function getItems($offset, $count)
    {
        return $this->itemsDAO->listItems($offset, $count);
    }
    public function count()
    {
        return $this->itemsDAO->count();
    }
    
    private $itemsDAO;
}