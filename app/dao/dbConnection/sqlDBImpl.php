<?php
require_once 'sqlDB.php';
require_once 'app/Log.php';
class mySQLConnection implements sqlDB
{
    private static
        $server_name = "localhost",
        $db_name = "mvcdb",
        $username = "mvcUser",
        $password = "12345",
        $port = "3306";

    private $connection;
    public function __construct()
    {
        $this->connection = new mysqli(
            self::$server_name,
            self::$username,
            self::$password,
            self::$db_name, 
            self::$port);
    }
    public function query($query)
    {
        Logger::log("QUERY: " . $query);
        return $this->connection->query($query);
    }
    public function close()
    {
        $this->connection->close();
    }
}