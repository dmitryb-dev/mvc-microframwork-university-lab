<?php
interface sqlDB {
    public function __construct();
    public function query($query);
    public function close();
}