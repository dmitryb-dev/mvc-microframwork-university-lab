<?php
interface userDAO
{
    public function load($email);
    public function loadById($id);
    public function add($user);
}