<?php
require_once 'dbConnection/sqlDBImpl.php';
require_once 'itemDAO.php';
require_once 'app/model/item.php';
class ItemDAOImpl implements ItemDAO
{
    public function load($id)
    {
        $connection = new mySQLConnection();
        $result = $connection->query('SELECT * FROM items WHERE iditem = \'' . $id . '\'');
        $connection->close();

        $item = null;
        if ($result != null && ($row = $result->fetch_assoc()) != null)
        {
            $item = new Item($row['iditem'], $row['name'], $row['description'], $row['category'], $row['img'], $row['price']);
        }
        return $item;
    }
    public function add($item)
    {
        $connection = new mySQLConnection();
        $connection->query('INSERT INTO items (name, description, category, img, price) VALUES('
            . '\'' . $item->name . '\', \''
            . $item->description . '\', \''
            . $item->category . '\', \''
            . $item->img . '\', \''
            . $item->price . '\')'
        );
        $connection->close();
    }
    public function remove($id)
    {
        $connection = new mySQLConnection();
        $result = $connection->query('DELETE * FROM items WHERE iditem = \'' . $id . '\'');
        $connection->close();
    }
    public function listItems($offset, $count)
    {
        $connection = new mySQLConnection();
        $result = $connection->query('SELECT * FROM items LIMIT ' . $offset . ', ' . $count);
        $connection->close();

        $items = array();
        if ($result != null)
        {
            while (($row = $result->fetch_assoc()) != null)
            {
                $item = new Item($row['iditem'], $row['name'], $row['description'], $row['category'], $row['img'], $row['price']);
                $items[] = $item;
            }
        }
        return $items;
    }
    public function count()
    {
        $connection = new mySQLConnection();
        $result = $connection->query('SELECT COUNT(*) FROM items');
        $connection->close();
        $row = $result->fetch_assoc();
        return $row['COUNT(*)'];
    }

}