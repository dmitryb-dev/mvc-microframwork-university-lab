<?php
require_once "CartDAO.php";
require_once "app/model/cart.php";
class CartDAOCookie implements CartDAO
{

    public function forUser($id = null)
    {
        if (!isset($_COOKIE['cart'])) return new Cart($id);
        $cart = new Cart(null, $this->parseItems($_COOKIE['cart']));
        $cart->setContact($_COOKIE['contact']);
        return $cart;
    }

    public function save($cart)
    {
        setcookie('cart', $this->getItemsString($cart), time() + 3600 * 24 * 30, '/');
        setcookie('contact', $cart->getContact(), time() + 3600 * 24 * 30, '/');
    }

    private function parseItems($string)
    {
        $items = explode(';', $string);
        $itemsArray = array();
        foreach ($items as $item)
            if ($item != '') $itemsArray[] = $item;
        return $itemsArray;
    }
    
    private function getItemsString($cart) 
    {
        $string = '';
        foreach ($cart->getItems() as $item)
            $string .= $item . ';';
        return $string;
    }
}