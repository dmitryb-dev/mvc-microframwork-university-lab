<?php
interface ItemDAO
{
    public function load($id);
    public function add($item);
    public function remove($id);
    public function listItems($offset, $count);
    public function count();
}