<?php
require_once 'dbConnection/sqlDBImpl.php';
require_once 'userDAO.php';
require_once 'app/model/user.php';
class userDAOImpl implements userDAO
{
    public function load($email)
    {
        $connection = new mySQLConnection();
        $result = $connection->query('SELECT id, email, pass FROM users WHERE email = \'' . $email . '\'');
        $connection->close();
        
        return self::readUser($result);
    }

    public function loadById($id)
    {
        $connection = new mySQLConnection();
        $result = $connection->query('SELECT id, email, pass FROM users WHERE id = \'' . $id . '\'');
        $connection->close();

        return self::readUser($result);
    }

    private function readUser($result)
    {
        $user = null;
        if ($result != null && ($row = $result->fetch_assoc()) != null) {
            $user = new User($row['id'], $row['email'], $row['pass']);
        }
        return $user;
    }

    public function add($user)
    {
        $connection = new mySQLConnection();
        $connection->query('INSERT INTO users (email, pass) VALUES('
            . '\'' . $user->email . '\', \'' . $user->pass . '\')'
        );
        $connection->close();
    }
}