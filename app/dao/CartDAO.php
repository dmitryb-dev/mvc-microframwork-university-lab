<?php

interface CartDAO
{
    public function forUser($id);
    public function save($cart);
}