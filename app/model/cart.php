<?php

class Cart
{
    private $userId, $items, $contact;

    public function getItems()
    {
        return $this->items;
    }

    public function __construct($userId, $items = null)
    {
        $this->userId = $userId;
        $this->items = $items == null? array() : $items;
    }

    public function addItem($itemId)
    {
        $this->items[] = $itemId;
    }
    public function removeItem($itemId)
    {
        $i = 0;
        foreach ($this->items as $item)
        {
            if ($item == $itemId)
            {
                unset($this->items[$i]);
                return true;
            }
            $i++;
        }
        false;
    }
    public function clear()
    {
        $this->items = array();
    }

    public function getContact()
    {
        return $this->contact;
    }
    public function setContact($contact)
    {
        $this->contact = $contact;
    }


}