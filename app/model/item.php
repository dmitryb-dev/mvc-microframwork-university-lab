<?php
class Item
{
    public $id, $name, $category, $description, $img, $price;

    public function __construct($id, $name, $description, $category, $img, $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->category = $category;
        $this->description = $description;
        $this->img = $img;
        $this->price = $price;
    }
}