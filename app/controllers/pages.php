<?php
require_once 'app/View.php';
require_once 'app/services/security/security.php';
require_once 'app/services/security/user_permission.php';
require_once 'app/services/itemsService.php';
require_once 'app/services/CartService.php';
class pages
{
    public function index()
    {
        return View::layout('Главная', 'index');
    }

    public function items()
    {
        if (!Security::permit(Permission::$USER)) return View::layout("Доступ закрыт", 'deny');

        $page = 0;
        if (isset($_GET['page'])) $page = $_GET['page'] - 1;

        $itemsService = new ItemsService();
        $model = array();
        $model['items'] = $itemsService->getItems($page * 3, 3);
        $model['count pages'] = ($itemsService->count() - 1) / 3 + 1;
        $model['page'] = $page + 1;
        return View::layout("Товары", "items", "Template", $model);
    }

    public function cart()
    {
        if (!Security::permit(Permission::$USER)) return View::layout("Доступ закрыт", 'deny');

        $itemsService = new CartService();
        $model = array();
        $model['items'] = $itemsService->getItems();
        $model['contact'] = $itemsService->getContactsForUser();
        return View::layout("Товары", "cart", "Template", $model);
    }

    public function signUp()
    {
        return View::layout("Регистрация...", 'signup');
    }
    public function signIn()
    {
        return View::layout("Вход...", 'signin');
    }

    public function register()
    {
        return View::layout('Регистрация', 'register');
    }
    public function login()
    {
        return View::layout("Вход", 'login');
    }
    public function logout()
    {
        return View::layout("logout", 'logout');
    }
}