<?php
require_once 'app/View.php';
require_once 'app/services/security/security.php';
require_once 'app/services/security/user_permission.php';
require_once 'app/services/itemsService.php';
require_once 'app/services/CartService.php';
class buy
{

    public function add()
    {
        if (!Security::permit(Permission::$USER)) return View::layout("Доступ закрыт", 'deny');

        $itemsService = new CartService();
        $itemsService->addItem($_GET['id']);
        return 'true';
    }

    public function remove()
    {
        if (!Security::permit(Permission::$USER)) return View::layout("Доступ закрыт", 'deny');

        $itemsService = new CartService();
        $itemsService->removeItem($_GET['id']);
        
        return 'true';
    }

    public function contact()
    {
        if (!Security::permit(Permission::$USER)) return View::layout("Доступ закрыт", 'deny');

        $itemsService = new CartService();
        $itemsService->setContactsForUser($_GET['contact']);

        return 'true';
    }

}