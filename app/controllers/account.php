<?php
require_once 'app/services/security/account.php';
require_once 'app/model/user.php';
require_once 'app/services/security/security.php';
class Account
{

    public function authenticate()
    {
        $this->accountService->authenticate($_GET['email'], $_GET['pass']);
    }

    public function signIn()
    {
        $token = $this->accountService->signIn($_GET['email'], $_GET['pass']);
        if ($token != null) $token->saveToCookie();
        echo $token != null? 'true' : 'false';
    }

    public function test() {
        Security::permit(Permission::$USER);
    }

    public function signUp()
    {
        $user = new User(0, $_GET['email'], $_GET['pass']);
        echo $this->accountService->register($user)? 'true' : 'false';
    }

    public function logOut()
    {
        $cookies = array_keys($_COOKIE);
        foreach ($cookies as $cookie)
            setcookie($cookie, "", 1, '/');
    }

    public function __construct()
    {
        $this->accountService = new AccountService();
    }
    
    private $accountService;
}