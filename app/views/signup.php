<div class="row" style="margin-top: 50px">
    <div class="col-md-8 col-md-offset-2">
        <h1 id="info" align="center">Регистрирую...</h1>
    </div>
</div>
<p>
    На самом деле я немного обманул. Это никакой не AccountController. Дело в том, что этот контроллер работает по
    схеме REST и при этом не выдаёт никакой страницы в ответ. Поэтому мы попали на другую страницу из контроллера pages
    на которой находится JS скрипт, который уже в свою очередь обращается к Account.
</p>

<p>
    По хорошему, здесь неплохо было бы сделать редирект... Но ведь тогда вы бы не увидели этого сообщения, верно?
</p>

<script src="../js/account.js"></script>
<script>
    var email = "<?php echo $_GET['email']?>";
    var pass = "<?php echo $_GET['pass']?>";


    var callback = function(res) {
        document.getElementById('info').innerHTML = res?
            'Регистрация успешна' : 'Такой пользователь уже существует';
        if (res)
            signIn(email, pass, null);
    }
    signUp(email, pass, callback);
</script>

<div class="col-md-6 col-md-offset-3">
    <a href="../pages/items"><button class="btn btn-primary btn-block">Окей, теперь к товарам</button></a>
</div>
