<div class="row" style="margin-top: 50px">
    <div class="col-md-8 col-md-offset-2">
        <h1 id="info" align="center">Выход...</h1>
    </div>
</div>

<script src="../js/account.js"></script>
<script>
    logout(function () {
        window.location.replace("../pages/index");
    });
</script>

