<div class="row" style="margin-top: 10px">
    <div class="col-md-8 col-md-offset-2">
        <h1 align="center">Товары</h1>
    </div>
</div>
<script src="../js/request.js"></script>
<?php
    foreach ($model['items'] as $item)
    {
        ?>
        <div class="row" style="margin-top: 20px">
            <img class="col-md-3 col-md-offset-2" src="../<?php echo $item->img ?>"/>
            <div class="col-md-6">
                <h3><?php echo $item->name ?></h3>
                <p><?php echo $item->description ?></p>
                <h4>Цена: <?php echo $item->price ?> грн</h4>
                <div class="col-md-4">
                    <a href="../pages/items">
                        <button onclick="request('buy', 'add', <?php echo '\'id=' . $item->id . '\'' ?>)" class="btn btn-primary btn-block">В корзину</button>
                    </a>
                </div>
            </div>
        </div>
        <hr />
        <?php
    }
?>

<div class="text-center">
    <nav aria-label="Page navigation" >
        <ul class="pagination">
            <?php
            for ($i = 1; $i < $model['count pages']; $i++)
            {
            ?>
            <li><a <?php $state = '';
                    if ($model['page'] == $i) {
                        echo 'class = "active"';
                        $state = '<span class="sr-only">(current)</span>';
                    } ?>
                    href=<?php echo '"../pages/items?page=' . $i . '">' . $i . $state ?></a></li>
                <?php
                    }
                    ?>
    </ul>
</nav>
</div>

