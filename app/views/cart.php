<div class="row" style="margin-top: 10px">
    <div class="col-md-8 col-md-offset-2">
        <h1 align="center">Корзина</h1>
    </div>
</div>

<script src="../js/request.js"></script>
<form class="form-inline text-center">
    <div class="form-group">
        <label >Телефон:</label>
        <input type="text" class="form-control" id="contact" placeholder="Ваш телефон" value="<?php echo $model['contact']?>"/>
    </div>
    <div class="form-group">
        <a href="../pages/items">
            <button onclick="request('buy', 'contact', 'contact=' + document.getElementById('contact').value)"
            class="btn btn-default btn-block">сохранить</button>
        </a>
    </div>
</form>
<?php
    foreach ($model['items'] as $item)
    {
        ?>
        <div class="row" style="margin-top: 20px">
            <img class="col-md-3 col-md-offset-2" src="../<?php echo $item->img ?>"/>
            <div class="col-md-6">
                <h3><?php echo $item->name ?></h3>
                <p><?php echo $item->description ?></p>
                <h4>Цена: <?php echo $item->price ?> грн</h4>
                <div class="col-md-4">
                    <button onclick="request('buy', 'remove', <?php echo '\'id=' . $item->id . '\'' ?>)" class="btn btn-danger btn-block">Удалить</button>
                </div>
            </div>
        </div>
        <hr />
        <?php
    }
?>

<div class="col-md-6 col-md-offset-3">
    <button onclick="request('buy', 'remove', 'id=' <?php echo $item->id ?>)" class="btn btn-success btn-block">Оформить</button>
</div>