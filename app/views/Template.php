<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<head>
    <title><?php $title ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />

    <link href="../css/bootstrap.css" rel="stylesheet"/>
    <link href="../css/styles.css" rel="stylesheet"/>

    <!-- debug -->
    <link href="../../css/bootstrap.css" rel="stylesheet"/>
    <link href="../../css/styles.css" rel="stylesheet"/>
</head>
<body>

    <div class="head"/>
    <div class="container">
        <div class="row" >
            <div class="col-md-2 col-md-offset-2 site-name">MVC Demo</div>
            <nav class="col-md-8" style="color: white">
                <a href="../pages/index">Главная</a> |
                <a href="../pages/items">Товары</a> |
                <?php
                    if (Token::fromCookie() != null)
                    {
                        echo '<a href="../pages/cart">Корзина</a> | ';
                        echo '<a href="../pages/logout">Выйти</a>';
                    }
                    else
                    {
                        echo '<a href="../pages/login">Войти</a> | ';
                        echo '<a href="../pages/register">Зарегистрироваться</a>';
                    }
                ?>
            </nav>
        </div>

        <?php
        if ($content != null) include 'app/views/' . $content . '.php';
        ?>
    </div>

</body>
</html>