<div class="row" style="margin-top: 50px">
    <div class="col-md-8 col-md-offset-2">
        <h1 align="center">Вход</h1>
    </div>
</div>

<div class="col-md-6 col-md-offset-3">
    <form action="../pages/signIn">
        <div class="form-group">
            <label for="email">Ваш email:</label>
            <input type="email" class="form-control" id="email" placeholder="Email" name="email">
        </div>
        <div class="form-group">
            <label for="pass">И пароль:</label>
            <input type="password" class="form-control" id="pass" placeholder="Пароль" name="pass">
        </div>
        <button type="submit" class="btn btn-primary btn-block">Войти</button>
    </form>
</div>
