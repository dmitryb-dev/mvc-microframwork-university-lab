<div class="row" style="margin-top: 50px">
    <div class="col-md-8 col-md-offset-2">
        <h1 align="center">Регистрация</h1>
    </div>
</div>
<p>
    Я смотрю, вы решили обойти мой хитрый Security? Что ж... Сейчас вы будете иметь дело с достаточно простым кодом.
    При нажатии кнопки вы обратитесь уже к другому контроллеру, Account, он занимается всякими делами с пользователями.
    Ну как занимается. Он просто осбирает данные с форм и обрабатывает кнопочки - всю работу выполняет за него сервис, работу которого
    вы уже наблюдали ранее, когда вам отказали в доступе - AccountService.
</p>

<div class="col-md-6 col-md-offset-3">
    <form action="../pages/signUp">
        <div class="form-group">
            <label for="email">Ваш email:</label>
            <input type="email" class="form-control" id="email" placeholder="Email" name="email">
        </div>
        <div class="form-group">
            <label for="pass">И пароль:</label>
            <input type="password" class="form-control" id="pass" placeholder="Пароль" name="pass">
        </div>
        <button type="submit" class="btn btn-primary btn-block">Готово!</button>
    </form>
</div>
