function request(controller, method, params) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.open('GET', "../" + controller + "/" + method + "?" + params);
    xhr.onreadystatechange = function () {
        if (xhr.readyState != xhr.DONE) return;
        alert(xhr.responseText);
        window.location.reload();
    }
    xhr.send();
}
