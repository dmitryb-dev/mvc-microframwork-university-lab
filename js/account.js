
function signUp(email, pass, callback) {
    var xhr = new XMLHttpRequest();

    xhr.open('GET', '../account/signUp?' +
        'email=' + email +
        '&pass=' + pass,
        true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState != xhr.DONE) return;
        var res = xhr.status == 200 && xhr.responseText.indexOf('true') == 0;
        if (callback != null) callback(res);
    }
    xhr.send();
}

function signIn(email, pass, callback) {
    var xhr = new XMLHttpRequest();

    xhr.open('GET', '../account/signIn?' +
        'email=' + email +
        '&pass=' + pass,
        true);
    xhr.withCredentials = true;
    xhr.onreadystatechange = function () {
        if (xhr.readyState != xhr.DONE) return;
        var res = xhr.status == 200 && xhr.responseText.indexOf('true') == 0;
        if (callback != null) callback(res);
    }
    xhr.send();
}

function logout(callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "../account/logout");
    xhr.onreadystatechange = function () {
        if (xhr.readyState != xhr.DONE) return;
        if (callback != null) callback();
    }
    xhr.send();
}