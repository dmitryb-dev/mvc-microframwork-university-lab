<?php
require_once 'app/route/MVCRouter.php';
require_once 'app/Log.php';
require_once 'init.php';
Server::init();

Logger::clear();

$router = MVCRouter::getInstance();
$router->route($_SERVER['REQUEST_URI']);

echo Logger::getLogText();
