There is my project for university subject. Main task was a creating MVC based shop. 
So. I didn't use any framework or library. Even sessions have been implemented in myself. (Yes, I know about $_SESSION)

I use service-based architecture. For example i use only two tables. You can create DB using request in the file "mvcdb.sql".
And I use request router. Every request is intercepted and redirected to start.php.

I don't recommend use this system in real project due next reasons:

 - No cache. Even sessions frequently make database queries.

 - Security problems. Router class is very simple and it can have vulnerability.